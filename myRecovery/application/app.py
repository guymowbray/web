from flask import Flask, render_template
from flaskext.mysql import MySQL
from flask_restful import Api, Resource 

import unicodedata


app = Flask(__name__)
api = Api(app)


mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'password'
app.config['MYSQL_DATABASE_DB'] = 'mydb'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

@app.route('/')
def home():
   data = API().get_full_user_details()
   return render_template('home.html', data = data)


@app.route('/put')
def put():
   data = API().put()
   return render_template('home.html', data = data)

def teamLogicChecker(data):
    
    check_list = set(['surgeon', 'admin assistant', 'nurse']) # a set of required professions in a team

    lst = set()
    # make set of professions in input team 
    for i in data:
        # convert unicode to string
        text = unicodedata.normalize('NFKD', i['type']).encode('ascii', 'ignore')
        lst.add(text)

    if lst == check_list:
        RET = True
    else:
        RET = False
        
    return RET


class API(Resource):
    def __init__(self):
        self.conn = mysql.connect()
        self.cursor = self.conn.cursor()


    def get_users_annual_leave(self, date):
        # Find which date ids we are in
        self.cursor.execute("SELECT iddate FROM date WHERE date = %s;", date)
        date_id = str(self.cursor.fetchall()) #turn tuple into string
        date_id = date_id.replace("(", "").replace(")", "").replace(",", "")

        # Find users that have annual leave today
        query = "SELECT user_id FROM mydb.annual_leave \
                 WHERE %s BETWEEN start_date AND end_date \
                 OR %s = start_date \
                 OR %s = end_date;"

        self.cursor.execute(query, (date_id, date_id, date_id))
        user_ids = self.cursor.fetchall()

        # put it into a list
        lst_of_user_id = [] # list of users on annual leave
        for i in user_ids:
            i = str(i)
            i = i.replace("(", "").replace(")", "").replace(",", "")
            lst_of_user_id.append(int(i))

        return lst_of_user_id


    def get_user_speciality_list(self):
        query = "SELECT user_id, GROUP_CONCAT(speciality_name) as specialities FROM speciality_table \
        LEFT JOIN speciality_key \
        ON speciality_table.speciality_id = speciality_key.idspeciality_key \
        GROUP BY user_id;"
        
        self.cursor.execute(query)
        user_specialities = self.cursor.fetchall()

        columns = [col[0] for col in self.cursor.description]
        user_specialities = [dict(zip(columns, row)) for row in user_specialities]

        return user_specialities


    def get_user_details_formated(self):
        query = "SELECT user, profile_picture, first_name, biography, last_name, profession AS type \
                 FROM users \
                 LEFT JOIN type_key \
                 ON users.type = type_key.idtype_key;"
        self.cursor.execute(query)
        data = self.cursor.fetchall()

        # convert into dictionary
        columns = [col[0] for col in self.cursor.description]
        data = [dict(zip(columns, row)) for row in data]

        return data


    def get_full_user_details(self, date='2019-09-22'):

        annual_leave_list = self.get_users_annual_leave(date)

        user_specialities_list = self.get_user_speciality_list()
        
        user_detail_list = self.get_user_details_formated()

        # Join the user_specialities with the user details
        for i in user_detail_list:
            for j in user_specialities_list:
                if i['user'] == j['user_id']:
                    i['specialities'] = j['specialities']

        # Join the annual_leave_list with the user details
        for i in user_detail_list:
            if i['user'] in annual_leave_list:
                i['onLeave'] = 'Yes'
            else:
                i['onLeave'] = 'No'
       
        return user_detail_list


    def put(self):
        data = self.get_user_details_formated()
        
        # data = [{u'first_name': u'Dexter', u'last_name': u'Marquez', u'profile_picture': u'https://avatars1.githubusercontent.com/u/6820?s=400&v=4', u'user': 1, u'type': u'surgeon', u'biography': u'Troublemaker. Lifelong zombieaholic. Explorer. Creator. Travel buff. Bacon practitioner. General coffee ninja.'}]

        RET = teamLogicChecker(data)

        if RET:
            # NOTE: it should write to the team_schedules table with all of the user_ids and the team number.
            query = "SELECT * FROM users"

            self.cursor.execute(query)
            RET_OUTCOME = 'RET_PASS'
        elif not RET:
            RET_OUTCOME = 'RET_FAIL'

        return RET_OUTCOME
    


api.add_resource(API, '/users/<int:id>', endpoint = 'user')

if __name__ == '__main__':
    app.run()