-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `mydb`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `mydb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `mydb`;

--
-- Table structure for table `annual_leave`
--

DROP TABLE IF EXISTS `annual_leave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annual_leave` (
  `idannual_leave` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `start_date` int(11) NOT NULL,
  `end_date` int(11) NOT NULL,
  PRIMARY KEY (`idannual_leave`),
  UNIQUE KEY `idannual_leave_UNIQUE` (`idannual_leave`),
  KEY `fk_annual_leave_date_idx` (`start_date`),
  KEY `fk_annual_leave_date1_idx` (`end_date`),
  KEY `fk_annual_leave_user_id_idx` (`user_id`),
  CONSTRAINT `fk_annual_leave_date` FOREIGN KEY (`start_date`) REFERENCES `date` (`iddate`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_annual_leave_date1` FOREIGN KEY (`end_date`) REFERENCES `date` (`iddate`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_annual_leave_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annual_leave`
--

LOCK TABLES `annual_leave` WRITE;
/*!40000 ALTER TABLE `annual_leave` DISABLE KEYS */;
INSERT INTO `annual_leave` VALUES (1,1,1,2),(2,2,3,4),(3,3,1,2);
/*!40000 ALTER TABLE `annual_leave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `date`
--

DROP TABLE IF EXISTS `date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `date` (
  `iddate` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  PRIMARY KEY (`iddate`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `date`
--

LOCK TABLES `date` WRITE;
/*!40000 ALTER TABLE `date` DISABLE KEYS */;
INSERT INTO `date` VALUES (1,'2019-09-22'),(2,'2019-09-23'),(3,'2019-09-24'),(4,'2019-09-25');
/*!40000 ALTER TABLE `date` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `speciality_key`
--

DROP TABLE IF EXISTS `speciality_key`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `speciality_key` (
  `idspeciality_key` int(11) NOT NULL AUTO_INCREMENT,
  `speciality_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idspeciality_key`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `speciality_key`
--

LOCK TABLES `speciality_key` WRITE;
/*!40000 ALTER TABLE `speciality_key` DISABLE KEYS */;
INSERT INTO `speciality_key` VALUES (1,'orthopaedics'),(2,'renal'),(3,'paediatrics');
/*!40000 ALTER TABLE `speciality_key` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `speciality_table`
--

DROP TABLE IF EXISTS `speciality_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `speciality_table` (
  `idspeciality_table` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `speciality_id` int(11) NOT NULL,
  PRIMARY KEY (`idspeciality_table`),
  KEY `fk_speciality_table_speciality_key1_idx` (`speciality_id`),
  KEY `fk_speciality_table_table11_idx` (`user_id`),
  CONSTRAINT `fk_speciality_table_speciality_key1` FOREIGN KEY (`speciality_id`) REFERENCES `speciality_key` (`idspeciality_key`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_speciality_table_table11` FOREIGN KEY (`user_id`) REFERENCES `users` (`user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `speciality_table`
--

LOCK TABLES `speciality_table` WRITE;
/*!40000 ALTER TABLE `speciality_table` DISABLE KEYS */;
INSERT INTO `speciality_table` VALUES (1,1,1),(2,2,1),(3,3,1),(4,3,2),(5,3,4),(6,5,1),(7,6,1),(8,6,2),(9,7,1),(10,7,3);
/*!40000 ALTER TABLE `speciality_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams_schedule`
--

DROP TABLE IF EXISTS `teams_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams_schedule` (
  `idteams` int(11) NOT NULL AUTO_INCREMENT,
  `team_number` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  PRIMARY KEY (`idteams`),
  KEY `fk_teams_schedule_date1_idx` (`date`),
  KEY `fk_teams_schedule_table11_idx` (`user`),
  CONSTRAINT `fk_teams_schedule_date1` FOREIGN KEY (`date`) REFERENCES `date` (`iddate`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_teams_schedule_table11` FOREIGN KEY (`user`) REFERENCES `users` (`user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams_schedule`
--

LOCK TABLES `teams_schedule` WRITE;
/*!40000 ALTER TABLE `teams_schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `teams_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_key`
--

DROP TABLE IF EXISTS `type_key`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_key` (
  `idtype_key` int(11) NOT NULL AUTO_INCREMENT,
  `profession` varchar(45) NOT NULL,
  PRIMARY KEY (`idtype_key`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_key`
--

LOCK TABLES `type_key` WRITE;
/*!40000 ALTER TABLE `type_key` DISABLE KEYS */;
INSERT INTO `type_key` VALUES (1,'surgeon'),(2,'nurse'),(3,'admin assistant');
/*!40000 ALTER TABLE `type_key` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user` int(11) NOT NULL AUTO_INCREMENT,
  `profile_picture` varchar(255) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `biography` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user`),
  KEY `fk_users_type_key1_idx` (`type`),
  CONSTRAINT `fk_users_type_key1` FOREIGN KEY (`type`) REFERENCES `type_key` (`idtype_key`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'https://avatars1.githubusercontent.com/u/6820?s=400&v=4','Dexter','Marquez',1,'Troublemaker. Lifelong zombieaholic. Explorer. Creator. Travel buff. Bacon practitioner. General coffee ninja.'),(2,'https://avatars3.githubusercontent.com/u/1703387?s=400&v=4','Elijah','Buckley',2,'Introvert. Alcohol nerd. Twitteraholic. Extreme problem solver. Social media trailblazer.'),(3,'https://avatars0.githubusercontent.com/u/9684139?s=400&v=4','Alissia','Stanley',2,'Lifelong tv fanatic. Twitter buff. Zombie geek. Pop culture fan. Friendly web fanatic.'),(4,'https://avatars2.githubusercontent.com/u/48950?s=400&v=4','Justin','Macfarlane',3,'Hipster-friendly tv geek. Creator. Troublemaker. Internet enthusiast. Extreme bacon aficionado.'),(5,'https://avatars2.githubusercontent.com/u/5201004?s=400&v=4','Aniqa','Wang',1,'Extreme social media fanatic. Hipster-friendly internet enthusiast. Food specialist.'),(6,'https://avatars2.githubusercontent.com/u/1883082?s=400&v=4','Bianka','Edge',2,'Zombie practitioner. Hardcore beer specialist. Typical social media aficionado. Tv trailblazer.'),(7,'https://avatars1.githubusercontent.com/u/4589857?s=400&v=4','Alicja','Gill',2,'Infuriatingly humble student. Incurable web maven. Reader. Devoted communicator. Evil social media practitioner.'),(8,'https://avatars2.githubusercontent.com/u/9504864?s=400&v=4','Cataleya','Mahoney',3,'Certified writer. Music buff. Extreme reader. Travel junkie. Social media fanatic. Organizer. Professional food practitioner.');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-27  1:40:51
