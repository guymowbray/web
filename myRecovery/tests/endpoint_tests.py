import os
import tempfile

import pytest

from flaskr import flaskr


@pytest.fixture
def client():
    db_fd, flaskr.app.config['DATABASE'] = tempfile.mkstemp()
    flaskr.app.config['TESTING'] = True
    client = flaskr.app.test_client()

    with flaskr.app.app_context():
        flaskr.init_db()

    yield client

    os.close(db_fd)
    os.unlink(flaskr.app.config['DATABASE'])



def test_empty_db(client):
    res = [{u'first_name': u'Dexter', u'last_name': u'Marquez', u'profile_picture': u'https://avatars1.githubusercontent.com/u/6820?s=400&v=4', u'user': 1, 'specialities': u'orthopaedics', 'onLeave': 'Yes', u'type': u'surgeon', u'biography': u'Troublemaker. Lifelong zombieaholic. Explorer. Creator. Travel buff. Bacon practitioner. General coffee ninja.'}, {u'first_name': u'Aniqa', u'last_name': u'Wang', u'profile_picture': u'https://avatars2.githubusercontent.com/u/5201004?s=400&v=4', u'user': 5, 'specialities': u'orthopaedics', 'onLeave': 'No', u'type': u'surgeon', u'biography': u'Extreme social media fanatic. Hipster-friendly internet enthusiast. Food specialist.'}, {u'first_name': u'Elijah', u'last_name': u'Buckley', u'profile_picture': u'https://avatars3.githubusercontent.com/u/1703387?s=400&v=4', u'user': 2, 'specialities': u'orthopaedics', 'onLeave': 'No', u'type': u'nurse', u'biography': u'Introvert. Alcohol nerd. Twitteraholic. Extreme problem solver. Social media trailblazer.'}, {u'first_name': u'Alissia', u'last_name': u'Stanley', u'profile_picture': u'https://avatars0.githubusercontent.com/u/9684139?s=400&v=4', u'user': 3, 'specialities': u'orthopaedics,renal', 'onLeave': 'Yes', u'type': u'nurse', u'biography': u'Lifelong tv fanatic. Twitter buff. Zombie geek. Pop culture fan. Friendly web fanatic.'}, {u'first_name': u'Bianka', u'last_name': u'Edge', u'profile_picture': u'https://avatars2.githubusercontent.com/u/1883082?s=400&v=4', u'user': 6, 'specialities': u'renal,orthopaedics', 'onLeave': 'No', u'type': u'nurse', u'biography': u'Zombie practitioner. Hardcore beer specialist. Typical social media aficionado. Tv trailblazer.'}, {u'first_name': u'Alicja', u'last_name': u'Gill', u'profile_picture': u'https://avatars1.githubusercontent.com/u/4589857?s=400&v=4', u'user': 7, 'specialities': u'orthopaedics,paediatrics', 'onLeave': 'No', u'type': u'nurse', u'biography': u'Infuriatingly humble student. Incurable web maven. Reader. Devoted communicator. Evil social media practitioner.'}, {u'first_name': u'Justin', u'last_name': u'Macfarlane', u'profile_picture': u'https://avatars2.githubusercontent.com/u/48950?s=400&v=4', u'user': 4, 'onLeave': 'No', u'type': u'admin assistant', u'biography': u'Hipster-friendly tv geek. Creator. Troublemaker. Internet enthusiast. Extreme bacon aficionado.'}, {u'first_name': u'Cataleya', u'last_name': u'Mahoney', u'profile_picture': u'https://avatars2.githubusercontent.com/u/9504864?s=400&v=4', u'user': 8, 'onLeave': 'No', u'type': u'admin assistant', u'biography': u'Certified writer. Music buff. Extreme reader. Travel junkie. Social media fanatic. Organizer. Professional food practitioner.'}] 

    rv = client.get('/')
    assert b'No entries here so far' in rv.data

