# myrecovery Backend Code Test

Do not spend more than four hours on this test, it doesn't matter if the code is not fully-functional. We want to see how you go about problem-solving within constraints.

## Overview

1. Model a hospital's clinical 'teams'
2. Build a backend API that exposes a hospital's clinical teams via a single RESTful endpoint.
3. Send us your solution as either:
    i) a link to a fork of this repo
    ii) a git archive (`git archive --format=tar --output=backend-test.tar master`)

See 'Data models' below for an example of a team.

## Tooling

* Any recognised Python backend web framework, e.g. Django, Flask etc. (we use Django).

## Backend

### 1. Data models

Design the data models as you see fit, with a view to storing and representing the data effectively.

### 2. Team logic

A 'team' is be a group of individuals working with a surgeon. For example:

- Surgeon A
    - Nurse A
    - Nurse B
    - Admin Assistant A

- Surgeon B
    - Nurse B
    - Nurse C
    - Admin Assistant B

The logic for a team is as follows:

* There must be at least one surgeon and one nurse in a team
* There must be at most one surgeon and one admin assistant in a team
* Nurses and admin assistants can work in up to three teams
* Surgeons can only work in one team

Ensure the team logic is unit tested.

### 3. Endpoint

The endpoint must support the following fields:

* profilePicture
* firstName
* lastName
* type (e.g. Surgeon, Nurse, Admin Assistant)
* onLeave
* specialities (e.g. Orthopaedics, Renal, Paediatrics)
* biography

### Bonus

* Basic endpoint tests
